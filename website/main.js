import PixelGrid from './vendor/pixelGrid.js';

/* eslint-disable-next-line no-unused-vars */
const drawing = new Vue({
  el: '#drawing',
  data: {
    gridWidth: 8,
    gridHeight: 6,
    currentFrame: 1,
    framesNumber: 3,
  },
  mounted() {
    this.pixelGrid = new PixelGrid(this.$refs.pixelCanvas, this.gridWidth, this.gridHeight);
    this.pixelGrid.init();
  },
  methods: {
    updateWidth: function updateWidth(event) {
      if (!event.target.checkValidity()) {
        event.target.classList.add('warning');
      } else if (event.target.value !== '') {
        event.target.classList.remove('warning');
        this.gridWidth = event.target.valueAsNumber;
        this.pixelGrid.setGridWidth(this.gridWidth);
      }
    },
    updateHeight: function updateHeight(event) {
      if (!event.target.checkValidity()) {
        event.target.classList.add('warning');
      } else if (event.target.value !== '') {
        event.target.classList.remove('warning');
        this.gridHeight = event.target.valueAsNumber;
        this.pixelGrid.setGridHeight(this.gridHeight);
      }
    },
    draw: function draw(event) {
      const pixel = this.pixelGrid.getPixelPos(event.clientX, event.clientY);
      if (event.buttons === 1 && pixel && !this.pixelGrid.pixels[pixel.x][pixel.y]) {
        this.pixelGrid.drawPixel(pixel);
      } else if (event.buttons === 2 && pixel && this.pixelGrid.pixels[pixel.x][pixel.y]) {
        this.pixelGrid.clearPixel(pixel);
      }
    },
  },
});
