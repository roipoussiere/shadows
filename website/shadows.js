function polar2rect(r, a) {
  return [r * Math.cos(a), r * Math.sin(a)];
}

const myVar = polar2rect(2, 2);
console.log(myVar); // eslint-disable-line no-console
