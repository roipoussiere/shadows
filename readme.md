# Nowhere shadows

*This is a work in progress.*

This project is a web-page used to build a 3D structure projecting patterns on the ground, only with
its shadow. No batteries, no motor, no electronics.

This project was previously shared [on Thingiverse](https://www.thingiverse.com/thing:1253190), but
a better user interface and many scripts was needed for this project, so I decided to create my own
interface.

[**Go to the project website**](https://roipoussiere.frama.io/shadows/)

![](doc/images/website.png)

Features:
- draw custom drawing using a pixel grid;
- more stuff coming...

This project is inspired from my last work, the
[Customisable Shadow Display](https://github.com/roipoussiere/Customizable-Shadow-Display).

![](doc/images/simulation.jpg)

## Suggesting ideas or reporting bugs

Thank you to contribute! You can
[submit an issue️](https://framagit.org/roipoussiere/MastoZap/issues/new) on this GitLab repository.

If you are not familiar with the GitLab issue tracker, you can also [contact me](#contact).

## Coding

### 📚 Interesting readings

You can take a look at the
[Customisable Shadow Display readme](https://github.com/roipoussiere/Customizable-Shadow-Display).

### 📦 Dependencies

You need [npm](https://www.npmjs.com/) to install / work on this project:

```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
echo -e "npm version:\t$(npm -v)"
```

Note that the app doesn't use NodeJS: we only use npm for our build workflow, such as installing
dependencies.

### 🏗️  Installation

Clone this repository, then install with npm:

```bash
git clone https://framagit.org/roipoussiere/shadows.git
cd shadows
npm install
```

*install* creates a `vendor` directory, containing development version of third-party files (such as
frameworks or libraries).

### 🔨 Running tests

```bash
npm test
```

At this time, this will only run [ESLint](https://eslint.org/) on all JavaScript files, according to
the [airbnb rules](https://github.com/airbnb/javascript).

### 🎬 Starting website

```bash
npm start
```

This will run a built-in web server. The website will be available on https://127.0.0.2:8080 by
default (you can change the port with `npm config set shadows:port <port>`).

### 🎁 Creating a merge request

1. follow the *Dependencies* and *Installation* steps above;
2. [create a fork of this project](https://framagit.org/roipoussiere/shadows/forks/new);
3. copy the git https URL of your fork (between the *fork* and *download* button, ie.
`https://framagit.org/YOUR_USERNAME/shadows.git`);
4. set your fork as the new upstream url
(`git remote set-url origin https://framagit.org/YOUR_USERNAME/shadows.git`);
5. create a new branch (`git checkout -b myPatch`);
6. take some coffee, write code, test it (`npm test`) and commit;
7. push your work to the remote repository (`git push origin myPatch`);
8. got to the [main project page](https://framagit.org/roipoussiere/shadows) and click on the
button *Send merge request*.

Thank you! 💜

### 🌍 Using in production

Use `prod` npm environment variable:

```bash
env=prod npm install
```

- *install* build the `public`, ready to serve with your favorite web server, by:
  1. copying `website` directory to `public`;
  2. minifying Javascript and CSS files;
  3. creating a `vendor` directory, containing production version of third-party files.

## Contact

Hi! 👋 You can contact me:

- 🐘 [on Mastodon](https://mastodon.tetaneutral.net/@roipoussiere);
- 📧 by mail: roipoussiere at protonmail dot com

This project is published under the [MIT licence](LICENCE).